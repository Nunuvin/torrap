# LICENSE General Public License Version 3
# However please give credit to the creator of the original code 
# Made by Nunuvin
# Provided on AS IS basis without any warranty of any kind, either expressed or implied
# Use it at your own risk
# Torrap
# Do not remove the header from this file or other files created from it

win=False

print 'This is a simple puzzle, enjoy. \n','Type Q to exit'

while (win==False):
	Pinput=raw_input()
	torrap=[]
	for i in reversed(Pinput):
		torrap.append(i)
	torrap=''.join(torrap)
	print torrap
	if torrap==Pinput:
		print 'NIW UOY'
		print ' Thanks for playing! \n','press enter to finish!'
		wait=raw_input()
		win=True
	elif Pinput=='Q':
		win=True
	else:
		print 'Try again!'